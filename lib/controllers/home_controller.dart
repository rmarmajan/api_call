import 'package:api_test/constants/app_constant.dart';
import 'package:api_test/services/api_services.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  Rx<bool> loading = true.obs;

  RxList categories = [].obs;

  getData() async {
    var res = await ApiService.fetchData(url: '${AppConstant.apiBaseUrl}/home');
    dynamic data = res['data'];

    categories.value = data['categories'];

    loading.value = res['loading'];
  }
}
