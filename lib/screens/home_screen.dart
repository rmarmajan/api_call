import 'package:api_test/controllers/home_controller.dart';
import 'package:api_test/widgets/item_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({super.key});

  final homeController = Get.put(HomeController());

  @override
  Widget build(BuildContext context) {
    homeController.getData();
    return Scaffold(
      body: SafeArea(
          child: Obx(
        () => homeController.loading.value
            ? const Center(child: CircularProgressIndicator())
            : ListView(
                children: homeController.categories.map<Widget>((item) {
                return ItemWidget(title: item['name']);
              }).toList()),
      )),
    );
  }
}
