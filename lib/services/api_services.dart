import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class ApiService {
  static fetchData({required url}) async {
    url = "$url?format=json";
    try {
      var res = await Dio().get(url);
      if ((res.data).isEmpty) {
        return {'data': null, 'loading': false};
      } else {
        return {'data': res.data, 'loading': false};
      }
    } catch (e) {
      debugPrint("Sorry API bolauna skena");
      debugPrint("Error: $e");
      return {'data': 'no-data', 'loading': false};
    }
  }
}
