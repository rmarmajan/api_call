import 'package:api_test/constants/size_constant.dart';
import 'package:flutter/material.dart';

class ItemWidget extends StatelessWidget {
  const ItemWidget({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: SizeConstant.defaultPadx, vertical: 15),
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.black26, width: 1))),
      child: Text(
        title,
        style: const TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
      ),
    );
  }
}
